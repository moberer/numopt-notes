#+TITLE: Numerical Optimization -- Exam Questions
#+AUTHOR: Max Oberaigner
#+OPTIONS: H:2 num:1
#+OPTIONS: toc:2
#+LATEX_HEADER: \newcommand{\R}{\mathbb{R}}

\newpage
* Book Examples
See TC

* Mathematical Preliminaries
** Question 1
a) Explain the definition of an induced matrix norm.
b) Explain the meaning of the 1, 2, $\infty$ matrix norms?
c) What are Schatten norms?

Answers:
a) An induced matrix norm is a matrix norm defined by two vector norms.
   That is given $||.||_{a}$ and $||.||_{b}$, the induced matrix norm $||A||_{a,b}$ is defined as
   $||A||_{a,b} = \max_{x}\{||Ax||_{b} : ||x||_{a} \leq 1 \} = \max_{x \neq 0} \frac{||Ax||_{b}}{||x||_{a}}$

b) The 1-norm is the largest absolute column sum norm.
   The 2-norm is the spectral norm. (Largest singular value of A).
   The $\infty$ -norm is the largest absolute row sum norm.
   (See: https://www.youtube.com/watch?v=EwWwP_TKXFw)

c) The Schatten norms are matrix norms, which operate using the singular values.
   In them, the an $l_{p}$ norm of the vector of singular values is computed, leading to the final result.
   That is $||A||_{S_{p}} = \left( \sum_{i=1}^{\min{m,n}} (\sigma_{i}(A))^{p}\right)^{\frac{1}{p}}$.
   For $p = 1$ we obtain the nuclear norm,
   for $p = 2$ the norm is equivalent to the Frobenius norm,
   for $p = \infty$ we obtain the 2-norm.

** Question 2
a) What is the spectral decomposition theorem of a symmetric $n \times n$ matrix?

Answer:
a) Theorem:
    Let $A \in \R^{n \times n}$ be a symmetric $n \times n$ matrix.
    Then there exists an orthogonal matrix $U \in \R^{n \times n}$ where $U^{T}U = UU^{T} = I$
    and a diagonal matrix $D = \diag(d_{1}, d_{2}, \hdot)$ for which $U^{T}AU = D$.

    Note:
    The columns of $U$ are an orthonormal basis comprised of eigenvectors of $A$.
    The diagonal elements of $D$ are the corresponding eigenvalues of $A$.

** Question 3
a) What is the linear approximation theorem?
b) What is the quadratic approximation theorem?

Answers:
a) Theorem: Let $f : U \to \R$ be a continously differentiable function over an open set $U \subseteq \R^{n}$,
   and let $x \in U, r > 0$ satisfy $B(x, r) \subseteq U$. Then there exists $\xi \in [x,y]$ such that
   $f(y) = f(x) + \nabla f(\xi)^{T}(y-x)$ and
   $f(y) = f(x) + \nabla f(x)^{T}(y-x) + o(||y-x||)$.

b) Theorem: Let $f : U \to \R$ be a twice continously differentiable function over an open set $U \subseteq \R^{n}$,
   and let $x \in U, r > 0$ satisfy $B(x, r) \subseteq U$. Then for any $y \in B(x,r)$ there exists $\xi \in [x,y]$ such that
   $f(y) = f(x) + \nabla f(x)^{T}(y-x) + \frac{1}{2}(y-x)^{T}\nabla^{2}f(\xi)(y-x)$ and
   $f(y) = f(x) + \nabla f(x)^{T}(y-x) + \frac{1}{2}(y-x)^{T}\nabla^{2}f(x)(y-x) + o(||y-x||^{2})$.

** Question 4
a) What is the fundamental theorem of calculus?
b) Show how it can be applied to a multivariate function $f(x)$ and to its gradient $\nabla f(x)$

Answers:
a) Theorem:
   Let $f$ and $F$ be real-valued functions defined on the closed interval $[a,b]$ such that $F$
   is continous on $[a,b]$ and the derivative of $F$ is $f$, i.e. $F'(x) = f(x)$ for almost all points.
   (That is up to finitely many points) in $[a, b]$. If $f$ is Riemann-integrable on $[a,b]$ then
    $F(b) - F(a) = \int_{a}^{b}f(x) dx = \int_{a}^{b}F'(x) dx$

b) We use a parametric function $g(t) = f(x + t(y - x))$ for $t \in [0,1]$:
   $f(y) - f(x) = g(1) - g(0) = \int_{0}^{1}g'(t)dt = \int_{0}^{1}\nabla f(x + t(y - x)) \cdot (y - x) dt$

* Linear Programming
** Question 5
a) What is linear programming?
b) Give the definition of linear programs in standard form.

Answers:
a) Linear programming is a special type of optimization problem,
   where both the objective function as well as the constraints are linear.

b) The standard form is $\min_{x} c^{T}x$ where $Ax = b$ and $x \geq 0$

** Question 6
a) Transform a given linear program ... into standard form.

Answer:
a) We use three methods:
   - Slack variables ($a_{i}^{T}x \leq b \Rightarrow a_{i}^{T}x + s_{i} = b, s_{i} \geq 0$)
   - Surplus Variables ($a_{i}^{T}x \geq b \Rightarrow a_{i}^{T}x - s_{i} = b, s_{i} \geq 0$)
   - Split unconstrained variables ($x_{j} = x_{j}^{+} - x_{j}^{-}$)

** Question 7
a) Give a graphical representation of a given linear program.
b) Graphically show its solution.

Answer:
- Draw level lines
- Draw boundrary conditions, direction.
- "Des unterste Eck isses".

** Question 8
a) What is the definition of a convex set?

Answer:
a) Definition:
    A set $X \in \R^{n}$ is convex if for any and all $x, y \in X$ and any $\lambda \in [0, 1]$,
    we have $\lambda x + (1 - \lambda)y \in X$.

** Question 9
a) What is the definition of a polyhedron?
b) What are extreme points?
c) What are vertices?
d) What are basic feasible solutions?

Answers:
a) Definition:
   A polyhedron is a set that can be described in the form $P = \{x \in \R^{n} : Ax \geq b\}$
   where $A$ is a $m \times n$ matrix and $b$ is in $\R^{m}$.

b) An extreme point is a "corner" of a polyhedron.
   Definition: A point $x \in C$, where $x \neq \lambda y + (1-\lambda)z$ and $z,y \in C$, $\lambda \in [0,1]$.

c) It is a possible, unique solution set for a LP in the polyhedron.
   (This is equivalent to the previous property).

d) A basic feasible solution is a basic solution, which satisfies all constraints.
   A basic solution is a vector, for which all equality constraints hold and
   there are $n$ linearly independent active constraints. (If in $\R^n$)
   (This is equivalent to the previous property).

** TODO Question 10
a) How can you generate a basic feasible solution?

Answer:
a) We use the following procedure:
   - Choose $m$ linearly independent columns $A_{\beta(1)}, \hdots, A_{\beta(m)}$
   - Let $x_{i} = 0$ for all $i \neq \beta(1), \hdots, \beta(m)$.
   - Solve the system of $m$ equations $Ax=b$ for the unknowns $x_{\beta(1)}, \hdots, x_{\beta(m)}$.

** Question 11
a) Given a linear program ... generate a basic feasible solution.

Answer:
a) See the previous question.

** Question 12
a) What is a feasible direction?
b) How is it constructed?
c) What is the reduced cost vector?

Answers:
a) A direction we can move a non-zero length in, and still stay within the constraints.
   Definition:
   Let $x \in P$. A vector $d \in \R^{n}$ is said to be a feasible direction at x,
   if there exists a $\theta > 0$ for which $x + \theta d \in P$

b) For one nonbasic variable: $d_j = 1$.
   For all other nonbasic variables: $d_{i} = 0$.
   For all basic variables: $d_{B} = -B^{-1} A_{j}$ where $B = [A_{\beta(1)}, \hdots, A_{\beta(m)}]$.


c) The reduced cost vector $\bar{c}$ is the cost for each nonbasic variable.\\
   $\bar{c}_{j} = c_{j} - c_{B}^{T}B^{-1}A_{j}$

** TODO Question 13
a) How is the step size computed to ensure feasibility when moving along a feasible direction?

Answer:
a) $c_{j}$ negative:
   Stepsize $\theta^{*} = min_{i=1,\hdots,m : d_{\beta(i)} < 0}(-\frac{x_{\bta(i)}}{d_{\beta(i)}})$, .

** Question 14
a) Describe the simplex method.
b) Which degrees of freedom does the method offer?
c) What are possible choices?

Answers:
a) We use the following algorithm:
   - Start with a basis $B$ consisting of the basic columns of $A$.
   - Compute the reduced cost for all nonbasic indices $j$.
     If all are nonnegative: Found solution;
     Else, choose some index $j$ where $\bar{c}_{j} < 0$.
   - Compute $u = B^{-1}A_{j}$.
     If no component of $u$ is positive, $\theta^{*} = \infty$ and the optimal cost is $-\infty$, terminating the algorithm.
     If some component of $u$ is positive, let $\theta^{*} = \min_{i=1,\hdots,m : u_{i >0}}{x_{\beta(i)}/u_{i}}$
   - Let $l$ be the chosen index in the previous step.
     Form a new basis by replacing $A_{\beta(l)}$ with $A_{j}$.
     If y is the new basic feasible solution, its basic variables are given by $y_{j} = \theta^{*}$
     and $y_{\beta(i)} = x_{\beta(i)} - \theta^{*}u_{i}$ for all $i \neq l$.

b) Firstly, we may choose any index $i$, where the reduced cost is negative.
   Secondly, multiple indices $l$ could acheive the minimum and we can pick.

c) Possible "pivoting rules" are:
   - Choose the "most negative" reduced cost.
   - Choose the column, where the cost decrease $\theta^{*}|\bar{c}_{j}|$ is the largest.
   - Choose the first possible value.
** TODO Question 15
Carry out one iteration of the simplex method to the linear program ...

Answer:

** Question 16
a) Describe a strategy to find an initial basic solution to a linear program in standard form.

Answer:
a) We can use the "big-M" method.
    We assume that $b \geq 0$ and introduce artificial variables $y \in \R^{m}$
    and consider the modified LP $\min_{x,y} c^{T}x + M \cdot 1^{T}y$ where $Ax + y = b$ and $x,y \geq 0$.
    For a sufficiently large (scalar) $M$, all y-values will be driven to 0 (which lets us properly solve the original LP).
    However, in this formulation we can always start at the simple basic solution of $x = 0$ and $y = b$.

* Optimality Conditions
** Question 17
a) What is the definition of a local and global minimum?
b) Give the first order necessary condition of optimality and prove it.
c) What are stationary points?
d) What is the second order necessary and sufficient conditions of optimality?
e) What are the conditions under which a function has a global minimizer or maximizer?

Answers:
a) Local Minimum: $\exists \epsilon: f(x) \leq f(\xi)$ for all $||x - \xi|| < \epsilon$ and $x, \xi \in C$
   Global Minimum: $f(x) \leq f(\xi)$ where $x, \xi \in C$

b) First order necessary condition:
   If $x$ is a local minimal point, $\nabla f(x) = 0$.

   Proof: (TODO: Refine)
   $f'(x) = \lim_{h\to0}\frac{f(x + h) - f(x)}{h}$
   As $x$ is a local minimum, $f(x + h) \geq f(x)$ and hence $f(x + h) - f(x) \geq 0$.
   Therefore $\frac{f(x + h) - f(x)}{h} \geq 0$ and hence $\lim_{h \uparrow 0}\frac{f(x + h) - f(x)}{h} \geq 0$.
   However, $\lim_{h \downarrow 0}\frac{f(x + h) - f(x)}{h} \leq 0$.

   The top and bottom boundrary of $f'(x)$ is $0$, hence $f'(x) = 0$.

c) Stationary points are points, where $f'(x) = 0$.

d) Second order necessary condition:
   If $x$ is a local minimal point, $\nabla^{2}f(x)$ is positive (semi-)definite.

   Second order sufficient condition:
   First order condition and second order necessary condition.

e) If $f$ is a continous function defined over a non-empty, compact set $C \subseteq \R^{n}$,
   then there exists a global minimum and maximum point.
   - $C$ is closed and compact.

** TODO Question 18
Consider the following optimization problem:
$\min_{x} f(x) = \hdots$ where $\hdots$

a) Compute all stationary points.
b) Classify them into local/global minima or saddle points.
c) Prove/disprove the existance of a global minimizer.

Answers:
a) First derivative = 0.
b) Second derivative. Pos./neg. definite.
c) TODO: Coercive (Optimality Conditions: 16)

** Question 19
The problem $\min_{x}\frac{1}{2}x^{T}Ax+b^{T}x+c$ is given.

a) Compute the gradient of $f$.
b) Compute the hessian of $f$.
c) Give a characterisation of its stationary points based on the definiteness of $A$.

Answers:
a) Gradient: $\nabla f(x) = Ax + b$
b) Hessian: $\nabla^{2}f(x) = A$
a) If $A$ is positive/negative semi-definite and $Ax = -b$: Global minimum/maximum.
   Since the function is quadratic: If $A$ is pos/neg definite: Global minimum/maximum.

* Lagrange Multiplier Theory
** Question 20
:PROPERTIES:
:ID:       c99570b7-ea4a-47b2-8762-fe14f1581bc2
:END:
a) What is the Lagrange multiplier theorem for equality constrained optimization problems?
b) Draw a simple example.
c) Explain why the gradients of the constraint functions need to be linearly independent.

Answers:
a) Theorem:
   Let $f$ and $h$ be continously differentiable functions over $\R^{n}$.
   Assume that $x^{*}$ is a regular local minimum of the problem.
   Then there exists a unique vector $\lambda^{*} = (\lambda^{*}_{1}, ..., \lambda^{*}_{m})$ called a Lagrange multiplier vector,
   such that $\nabla f(x^{*}) + \sum_{i=1}^{m}\lambda^{*}_{i} \nabla h_{i}(x^{*}) = 0$.

b) Simple example:
       [[attachment:_20220204_201308Screenshot_20220204_201253.png]]

c) The $h$ -gradients span a space, hence they act as basis.
   If they aren't linearly independent, we can't do a unique linear combination
   and hence the Lagrange multiplierer is not uniquely defined.

** Question 21
a) What are the KKT conditions?
b) What does the complementary slackness condition say?

Answers.
a) The KKT Conditions are:
   - $\nabla_{x} L(x^{*}, \lambda^{*}, \mu^{*}) = 0$
   - $\mu^{*}_{j} \geq 0$ for $i \in {1, \hdots, r}$
   - $\mu^{*}_{j}g_{j}(x^{*}) = 0$ for $i \in {1, \hdots, r}$

b) The complementary slackness condition (Either mu or g is zero):
   - $(\mu^{*})^{T}g(x^{*})=0$

** Question 22
Consider the linear program
$\min_{x} c^{T}x$ where $Ax = b$ and $x \geq 0$.

a) Write down the Lagrangian using multipliers $y$ for the equality constraint and $s$ for the inequality constraint.
b) Derive the KKT conditions.

\noindent
Answers:

\noindent
$h(x) = Ax - b$ (zero if ok).\\
$g(x) = -x$

a) $L(x, \lambda) = f(x) + \sum_{i=1}^{m}y_{i}h_{i}(x) + \sum_{j=1}^{r}s_{j} g_{i}(x)$ \\
   $L(x, \lambda) = c^{T}x + \sum_{i=1}^{m}y_{i}(Ax-b) + \sum_{j=1}^{r}s_{j} (-x)$

b) $\nabla_{x} L(x, \lambda) = c + y A^{T} - s$

   Hence the KKT are:\\
   $A^{T}y^{*} + c - s^{*} = 0$ \\
   $s^{*} = 0$ \\
   $(s^{*})^{T} x^{T}) = 0$

** TODO Question 23
Show how to solve the projection problem:
$\min_{x} \frac{1}{2}||x-y||^{2}$ where $Ax = b$

a) Write down the Lagrangian.
b) Give the KKT conditions.
c) Show how the problem is solved.

Answers:
a)
b)
c)

** TODO Question 24
Show how to compute the projection onto a half space:
$\min_{x} \frac{1}{2} ||x-y||^{2}$ where $a^{T}x \leq b$.

a) Write down the Lagrangian.
b) Give the KKT conditions.
c) Show how the problem is solved.

Answers:
a)
b)
c)

* Least Squares Problems
** Question 25
a) What are linear and non-linear least squares problems?

Answer:
a) Linear: $\min_{x} \frac{1}{2} ||Ax - b||$
   Non-Linear: $\min_{x} \frac{1}{2} ||g(x) - b||$

** TODO Question 26
a) How is the method used to solve overdetermined systems of linear equations?

Answer:
a) We try to fit

** TODO Question 27
a) Describe the method of linear least squares for data fitting / regression.
b) How can you determine the minimal number of data points that are necessary?

Answers:
a) Least squares minimizes the square of the distances of the data points.
b) The least amount of data points (for a unique solution) is the amount of dimensions.
* Gradient Methods
** Question 28
:PROPERTIES:
:ID:       05cd1fb0-83d2-45f0-a97c-f2b229bc6578
:END:
a) Give the definition of a descent direction.
b) Draw a simple example explaining the properties of a descent direction.

Answer:
a) Definition:
   Let $f : \R \to \R$ be a continously differentiable function over $\R^{n}$.
   A vector $0 \neq d \in \R^{n}$ is called a descent direction of $f$ at $x$
   if the directional derivative $f'(x; d)$ is negative, that is:
                       $\nabla f(x)^{T}d < 0$

b) The image is:
   [[attachment:_20220205_110008Screenshot_20220205_105835.png]]

** Question 29
a) Give the general form of a descent method.
b) Show that $d^{k} = -D^{k} \nabla f(x^{k})$ with $D^{k}$ symmetric and positive definite.
c) Give three different standard choices for descent directions based on choosing the scaling matrix $D^{k}$.
d) Discuss their performance.

Answer:
a) The general form:
   - Choose initial vector $x_{0}$
   - Choose descent direction $d^{k}$ that satisfies $\nabla f(x^{k})^{T}d^{k} < 0$
   - Choose a positive step size $t^{k}$ satisfying $f(x^{k} + t^{k} d^{k} < f(x^{k}$
   - Set the new vector $x^{k+1}$ accordingly
   - Stop or continue.

b) $y^{T} D y > 0$ as per definition of positive definite.
   Hence, $\nabla f(x^{k})^{T} D \nabla f(x^{k}) > 0$,
   which gives use $\nabla f(x^{k})^{T} (-d^{k}) > 0$
   and therefore $\nabla f(x^{k}) d^{k} < 0$.

c) Steepest descent: $D^k = I$ -- Pretty poor performance.
   Newtons Method: $D^{k} = (\nabla^{2} f(x^{k}))^{-1}$ -- Much better.
   Gauss-Newton (least squares only): $D^{k} = \nabla g(x^{k}) \nabla g(x^{k})^{T})^{-1}$ -- Even better.

d) See above.
** Question 30
a) Explain three methods how to select the step size in gradient methods.

Answer:
a) Methods are:
   - Minimization Rule -- Chose step size such that the function becomes minimal along the chosen direction.
   - Constant Step Size -- Just keep it the same; Optimal one can sometimes be computed from the given problem.
   - Diminishing Step Size -- Reduce it every step. Sum should not converge.
** Question 31
a) Prove the formula for exact line search based on a descent direction $d$
for quadratic functions of the form $\min_{x} \frac{1}{2} x^{T}Ax + b^{T}x+c$

Answer:
a) The quadratic formula is of the form $f(x) = \frac{1}{2}x^{T}Ax+b^{T}x+c$.
   A direction $d$ is given.
   Hence we can parametise to $f(x + td) = \frac{1}{2} (x+td)^{T}A(x+td)+b^{T}(x+td)+c$.
   Deriving this by $t$ gives us $\frac{\delta f(x+td)}{\delta t} = d^{T}A(x+td) + d^{T}b$.
   We set this zero  $d^{T}Ax + td^{T}A + d^{T}b = 0$,
   hence $d^{T}(Ax+b) + td^{T}A = 0$
   and $d^{T}(\nablaf(x)) + td^{T}A = 0$,
   which gives us $t = - \frac{d^{T}\nabla f(x)}{d^{T}Ad}$ Q.E.D.

** TODO Question 32
:PROPERTIES:
:ID:       053f4bf2-c85e-42d2-bffd-2d3e27255788
:END:
a) Explain the Armijo step size rule.
b) Draw a figure.
c) Prove the sufficient decrease condition and show its relation to the Amijo step size condition.

Answer:
a) The Amijo rule works as follows:
   - Fix an initial step size $s > 0$ and the parameters $\beta, \sigma \in [0,1]$
   - At every step choose a step size $t^{k} = s\beta^{m_{k}}$,
     where $m_{k}$ is a non-negative integer such that
     $f(x^{k})--f(x^k+t^kd^k) \geq - \sigma t^k\nabla f(x^k)^Td^k$ ($d_k$ being a valid descent direction).

    Interpretation: Step sizes are tried until we find one with suficient decrease

b) [[attachment:_20220205_131400Screenshot_20220205_131356.png]]

c) TODO Proof:
   $f(x+td) = f(x) + t \nabla f(x)^{T}d + o(t||d||)$
   $f(x) - f(x+td) = -t \nabla f(x)^Td - o(t||d||)$
   $= <t \nabla f(x^{T}df),>$
** Question 33
a) What is the "zig-zag" effect?
b) Prove that the differences of successive iterates are orthogonal when using exact line search.

Answers:
a) The gradients of successive steps tend towards being orthogonal to each other,
   the better the step size is chosen. This behaviour is at it's most extreme with the
   line search method.

b) Since the result point is a global minimum along the search line,
   it is clear that $d^{T} \nabla f(p) = 0$. This in turn means that the last search direction
   $d$ and the new gradient are orthagonal.

** Question 34
a) What is the rate of convergence of the gradient method for quadratic functions when using exact line search?
b) What is the condition number?

Answers:
a) $f(x^{k}) \leq (\frac{M-m}{M+m})^{2k}f(x_{0})$ where m is the smallest eigenvalue and M the largest.
b) The condition number of a matrix is $\kappa(A) = \lambda_{max}(A) / \lambda_{min}(A)$.
** Question 35
a) What is the Lipschitz continous gradient?
b) Show how it is related to the norm of the hessian matrix?
c) What is the Lipschitz constant of the gradient of the least squares problem $\min_{x}\frac{1}{2}||Ax-b||^{2}$.

Answers:
a) A function is Lipschitz continous if $||f(x) - f(y)|| \leq L||x - y||$.
   Hence, a Lipschitz continous gradient is a gradient with this property.
b) The Lipschitz constant of the gradient provides an upper bound to the operator norm of the hessian matrix.
c) Quadratic functions have a Lipschitz continous gradient with $L = ||A||_{2} = \lambda_{max}(A)$.
** TODO Question 36
a) Prove the descent lemma for a differentiable function $f$ with Lipschitz continous gradient.
   Interpret the inequality of the descent lemma in terms of an upper bound to the function $f$.
c) Use the descent lemma to also prove the sufficient decrease lemma.

Answers:
a) TODO
   Proof:
   We know $||\nabla f(x) - \nabla f(y)|| \le L||x-y||$.

   And hence, finally $f(y) \leq f(x) + \nabla f(x)^{T}(y-x) +\frac{L}{2}||x-y||^2$.
b)

** Question 37
a) What is the rate of convergence of the gradient method?

Answer:
$\min_{k=0,\hdots,n}||\nabla f(x^{k})||^{2} \leq \frac{f(x^{0}-f^{*})}{M(n+1)} = O(1/n)$

* Gauss-Newton Algorithm and Kalman Filter
** Question 38
a) Give the general form of the non-linear least squares problem.
b) Show how the Gauss-Newton method is obtained from performing a first-order Taylor approximation.
c) What is the Levenberg-Marquadt method?

Answers:
a) $\frac{1}{2}||g(x)||^2$
b) The first Taylor approximation of $g(x) \approx g(y) + \nabla g(y) (x - y)$
       Substituting back gives us $\frac{1}{2}||g(y) + \nabla g(y) (x - y)||$.
   Solving this gives us $x = y - (\nabla g(y) \nabla g(y)^{T})^{-1} \nabla g(y) g(y)$
c) Levenberg-Marquardt is a modified Gauss-Newton method,
       where the matrix inversion is made always possible by adding a diagonal matrix $\delta^{k}I$ before inverting.
       
** TODO Question 39
    a) Explain the Kalman filter.
    b) Show it's relation to the incremental Gauss-Newton method.
    c) What are its applications?

    Answers:
    a) The Kalman filter works via:
       $\xi_{i} = \xi_{i-1}+H_{i}^{-1}C_{i}^{T}(z_{i}-C_{i}\xi_{i-1})$
       $H_{i} = \lambda H_{i-1} + C_{i}^{T}C_{i}$
    b) TODO
    c) Navigation, etc.
* Newtons Method
** TODO Question 40
    a) Show that the plain form of Newton's method can be derived from a second order Taylor approximation of the objective function.
    b) Show that Newton's method is invariant with respect to affine scalings.

    Answers:
    a)
    b)
** TODO Question 41
    a) How can Newton's method be used to solve for the roots of nonlinear equations.

    Answers:
    a)
** TODO Question 42
    a) Given the theorem of locally quadratic convergence of the pure form of Newton's method and prove it.
    b) What can you say about the global convergence of Newton's method?

    Answers:
    a)
    b)
* Accelerated Gradient Methods
** TODO Question 43
    a) What is Q-conjugacy?
    b) Show how the Gram-Schmidt procedure can be used to generate a set of Q-conjugate directions
       from a set of linearly independent vector.
** TODO Question 44
    a) Write down the conjugate gradient method.
    b) What is its convergence rate?
    c) How can it be generalized to non-linear problems?
** TODO Question 45
    a) Explain the heavy-ball algorithm and show that it is obtained from a finite differences
        approximation of the heavy-ball with friction dynamical system.
    b) How is it related to the CG method?
* Optimization over Convex Sets
** TODO Question 46
    a) Give the definition of stationary points for minimizing a differentiable function over a convex set.
    b) Give an example showing the necessary optimality condition for minimizing a differentiable function
       over a convex set.
    c) Why does it fail in case the constraint set is non-convex.
** TODO Question 47
    a) Explain the projection on a convex set?
    b) Write down the projection theorem.
    c) Compute the projection of a point $z \in \R^{n}$ to the constraint set $C = \{x \in \R^{n} : Ax = 0\}$
** TODO Question 48
    a) Show that stationary in optimization over a convex set can also be written
       in terms of the projection operator $proj_{C}(x)$.
** TODO Question 49
    a) What is the gradient projection method.
    b) Give the algorithm and give choices for the step size selection.
    c) Draw an example for minimizing a quadratic function over the $l_{1}$ ball (in 2D),
       illustrating the iterations of the gradient projection method.
** TODO Question 50
    a) What is the scaled gradient projection method.
    b) Show how to specialize your algorithm such that it becomes a projected Newton algorithm.
** TODO Question 51
   a) What is the conditional gradient method?
    b) Draw a simple example in 2D, for minimizing a quadratic function over the $l_{1}$ ball.
    c) Show the computation of the extremal points as well as the iterations.
** TODO Question 52
    a) Show how the extremal points are computed in case the constrained set C is the unit simplex.
** TODO Question 53
    a) Explain three choices for selecting the step size.
** TODO Question 54
    a) What is the power method?
    b) Show that it can be derived from the conditional gradient method for maximizing the Rayleigh quotient.
